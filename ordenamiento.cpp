#include <iostream>
#include <stdlib.h>
using namespace std;

struct nodo{
       int nro;        // en este caso es un numero entero
       struct nodo *sgte;
};

typedef struct nodo *Tlista;

int posicionUltimoElemento(Tlista lista)
{
    int n=0;
    while(lista!=NULL){
        n++;
        lista=lista->sgte;
    }
    return n;
}

void insertarInicio(Tlista &lista, int valor)
{
    Tlista nuevo;
    nuevo = new(struct nodo);
    nuevo->nro = valor;
    nuevo->sgte = lista;
    lista  = nuevo;
}

void insertarFinal(Tlista &lista, int valor)
{
    Tlista nuevo = new(struct nodo);
    nuevo->nro  = valor;
    nuevo->sgte = NULL;

    if(lista==NULL)
    {
        lista = nuevo;
    }
    else
    {
        Tlista p = lista;
        while(p->sgte!=NULL)
        {
            p = p->sgte;
        }
        p->sgte = nuevo;
    }

}

void insertarElemento(Tlista &lista, int valor, int pos)
{
    if(pos==1)
    {
        insertarInicio(lista,valor);
    }
    else
    {
        Tlista nuevo;
        int n;
        nuevo = new(struct nodo);
        nuevo->nro = valor;

        Tlista p=lista;
        n=1;

        while(n!=pos-1){
            p=p->sgte;
            n=n+1;
        }
        nuevo->sgte=p->sgte;
        p->sgte=nuevo;
    }
    /**cout<<"   Error...Posicion no encontrada..!"<<endl;**/
}

void eliminarPrimerElemento(Tlista &lista)
{
    Tlista p=lista;
    lista=lista->sgte;
    delete(p);
}

void eliminarUltimoElemento(Tlista &lista)
{
    if(lista->sgte==NULL){
        eliminarPrimerElemento(lista);
    }
    else
    {
        Tlista p=lista,r=NULL;
        while(p->sgte!=NULL){
            r=p;
            p=p->sgte;
        }
        r->sgte=NULL;
        delete(p);
    }

}

void eliminarElemento(Tlista &lista, int pos)
{

    if(pos==1){
        eliminarPrimerElemento(lista);
    }
    else{
        Tlista p=lista, r=NULL;
        int n=1;
        while(n!=pos){
            r=p;
            p=p->sgte;
            n++;
        }

        r->sgte=p->sgte;
        delete(p);
    }

}

void ordenarBurbuja(Tlista lista)
{
    bool interruptor;
    Tlista q = NULL, r = NULL;
    if(lista==NULL){
        //cout<<"Lista vacia"<<endl;
    }else{
        if(lista->sgte==NULL){
            cout<<"Unico elemento en lista, no es posible ordenamiento"<<endl;
        }else{
            do{
                q=lista;
                interruptor=false;
                do{
                    if(q->nro>q->sgte->nro){
                        int aux=q->sgte->nro;
                        q->sgte->nro=q->nro;
                        q->nro=aux;
                        interruptor=true;
                    }
                    q=q->sgte;
                }while(q->sgte!=NULL);
            }while(interruptor);
        }
    }
}

void ordenarSelecion(Tlista lista)
{   Tlista p=NULL,q=NULL;
    int m;
    if(lista==NULL){
        //cout<<"Lista vacia"<<endl;
    }else{
        if(lista->sgte==NULL){
            cout<<"Unico elemento en lista, no es posible ordenamiento"<<endl;
        }else{
            while(lista->sgte!=NULL){
                p=lista;
                m=lista->nro;
                q=lista;
                while(p->sgte!=NULL){
                    if(p->sgte->nro<m){
                        m=p->sgte->nro;
                        q=p->sgte;
                    }
                    p=p->sgte;
                }
                q->nro=lista->nro;
                lista->nro=m;
                lista=lista->sgte;
            }
        }
    }

}

void ordenarInsercion(Tlista &lista,int valor)
{
    int n=1;
    if(lista==NULL){
        insertarInicio(lista,valor);
    }
    else{
       while(lista->sgte!=NULL && valor>lista->nro ){
            n++;
            lista=lista->sgte;
        }
        if(n==1){
            if(lista->nro<valor)
                insertarFinal(lista, valor);
            else{
                insertarInicio(lista,valor);
            }
        }else{
            if(n==posicionUltimoElemento(lista))
                insertarFinal(lista,valor);
            else
                insertarElemento(lista,valor,n-1);
        }
    }

}

void ordenarIntercambio(Tlista lista)
{
    Tlista p=NULL;
    int aux;

    if(lista==NULL){
        //cout<<"Lista vacia"<<endl;
    }else{
        if(lista->sgte==NULL){
            cout<<"Unico elemento en lista, no es posible ordenamiento"<<endl;
        }else{
            while(lista->sgte!=NULL){
                p=lista;

                while(p->sgte!=NULL){
                    if(p->sgte->nro<lista->nro){
                        aux=lista->nro;
                        lista->nro=p->sgte->nro;
                        p->sgte->nro=aux;
                    }
                    p=p->sgte;
                }
                lista=lista->sgte;
            }

        }
    }




}

void mostrarLista(Tlista lista)
{
     if(lista==NULL){
        cout<<"\n\tLista vacia!"<<endl;
     }
     else
     {
        int n = 0;
        while(lista != NULL)
        {
          cout <<' '<< n+1 <<") " << lista->nro << endl;
          lista = lista->sgte;
          n++;
        }
     }

}


void menu1()
{
    cout<<"\n\t\tLISTA ENLAZADA SIMPLE\n\n";
    cout<<" 1 . INSERTAR AL INICIO               "<<endl;
    cout<<" 2 . INSERTAR AL FINAL                "<<endl;
    cout<<" 3 . INSERTAR EN UNA POSICION         "<<endl;
    cout<<" 4 . ELIMINAR EL PRIMER ELEMENTO      "<<endl;
    cout<<" 5 . ELIMINAR ULTIMO ELEMENTO         "<<endl;
    cout<<" 6 . ELIMINAR ELEMENTO POR POSICION   "<<endl;
    cout<<" 7 . ORDENAMIENTO BURBUJA             "<<endl;
    cout<<" 8 . ORDENAMIENTO SELECCION           "<<endl;
    cout<<" 9 . ORDENAMIENTO INSERCION           "<<endl;
    cout<<" 10. ORDENAMIENTO INTERCAMBIO         "<<endl;
    cout<<" 11. MOSTRAR LISTA                    "<<endl;
    cout<<" 0 . SALIR                            "<<endl;

    cout<<"\n INGRESE OPCION: ";
}

/*                        Funcion Principal
---------------------------------------------------------------------*/

int main()
{
    Tlista lista = NULL;
    int op;     // opcion del menu
    int _dato;  // elemenento a ingresar
    int pos;    // posicion a insertar

    system("color 0b");

    do
    {
        menu1();  cin>> op;

        switch(op)
        {
            case 1:

                 cout<< "\n NUMERO A INSERTAR: "; cin>> _dato;
                 insertarInicio(lista, _dato);
                 mostrarLista(lista);

            break;


            case 2:

                 cout<< "\n NUMERO A INSERTAR: "; cin>> _dato;
                 insertarFinal(lista, _dato );
                 mostrarLista(lista);

            break;


            case 3:

                 cout<< "\n NUMERO A INSERTAR: ";cin>> _dato;
                 cout<< " Posicion : ";       cin>> pos;
                 while(pos<=0 || pos>posicionUltimoElemento(lista)+1){
                    cout<<"\n\t-->LA POSICION NO EXISTE!, ingrese una posicion correcta..."<<endl;
                    cout<< " Posicion : ";       cin>> pos;
                 }
                 insertarElemento(lista, _dato, pos);
                 mostrarLista(lista);

            break;


            case 4:
                if(lista==NULL){
                    cout<<"\n\t\t||Operacion invalida!||\n\n";
                }
                else{
                    eliminarPrimerElemento(lista);
                    cout<<"\n PRIMER ELEMENTO ELIMINADO..."<<endl;
                }
                mostrarLista(lista);

            break;


            case 5:
                if(lista==NULL){
                    cout<<"\n\t\t||Operacion invalida!||\n\n";
                }
                else{
                    eliminarUltimoElemento(lista);
                    cout<<"\n ULTIMO ELEMENTO ELIMINADO..."<<endl;
                }
                mostrarLista(lista);

            break;

            case 6:
                if(lista==NULL){
                    cout<<"\n\t\t||Operacion invalida!||\n\n";
                }
                else{
                    cout<< " Posicion : ";       cin>> pos;
                    while(pos<=0 || pos>posicionUltimoElemento(lista)){
                        cout<<"\n\t-->LA POSICION NO EXISTE!, ingrese una posicion correcta..."<<endl;
                        cout<< " Posicion : ";       cin>> pos;
                    }
                eliminarElemento(lista,pos);
                }
                mostrarLista(lista);

            break;

            case 7:
                ordenarBurbuja(lista);
                mostrarLista(lista);

                break;
            case 8:
                ordenarSelecion(lista);
                mostrarLista(lista);
                break;

            case 9:
                cout << "INSERTAR ELEMETO: ";
                cin>>_dato;
                ordenarInsercion(lista,_dato);
                mostrarLista(lista);
                break;

            case 10:
                ordenarIntercambio(lista);
                mostrarLista(lista);
                break;

            case 11:

                cout << "\n\n MOSTRANDO LISTA\n\n";
                mostrarLista(lista);
            break;

                    }

        cout<<endl<<endl;
        system("pause");  system("cls");

    }while(op!=0);


   system("pause");
   return 0;
}


